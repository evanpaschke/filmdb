# FilmDB Web Application

Web UI to view films provided by the MovieDB API.

## Features
The Film DB web app features a main page with film search and an infinite scroll of the most popular films ranked by the Movie DB. Clicking on a film from the main page shows the film detail page with an overview and other metadata.

The API is documented and validated using the OpenAPI definition (openapi.yml). Internally, the endpoints map to several Movie DB API endpoints. Requests to the Movie DB are made with an API key (see Configuration below) and are cached for one hour.

## Configuration
The FilmDB service is configured using environment variables and expects keys in the form `FILMDB_${NODE_ENV}_${KEY}`. Required environment variables are parsed and checked in the src/server/config.js file. The FilmDB service requires a `.env` file for local development with the keys below. The application will throw an error at startup if necessary config is missing. 

```
# .env file
FILMDB_API_KEY=<api key from movie db>
FILMDB_BASE_URL=https://api.themoviedb.org/3
DEBUG=filmdb
```

## Development

Run the node app locally:

```bash
npm run dev
```
Runs on [http://localhost:3000/](http://localhost:3000/)

Run the frontend locally:
```bash
npm run dev:ui
```
Runs on [http://localhost:1234/](http://localhost:1234/), proxies api requests to port 3000

## Execution
```bash
docker pull registry.gitlab.com/evanpaschke/filmdb
docker run --env-file .env -p 3000:3000 registry.gitlab.com/evanpaschke/filmdb
```
Runs app on: [http://localhost:3000/](http://localhost:3000/)

## Dependencies

Backend
- express - handle HTTP requests
- axios - make HTTP requests
  - axios-cache-adapter - caches requests made by the node app to the Movie DB API
- openapi-validator-middleware - validate request parameters against openapi schema definition
  - js-yaml - parse openapi definition in yaml
- morgan - log HTTP requests

Frontend
- react-router-dom - router for addressable film pages
- react-infinite-scroller - infinite scroll for film list
- theme-ui - theme-based component styling system


