export default {
    borders: {},
    borderStyles: ['solid'],
    borderWidths: [1],
    colors: {
        text: '#f5f8fa',
        placeholder: 'rgba(245, 248, 250, 0.66)',
        background: '#333',
        backgroundDark: '#222',
        midground: '#393939',
        primary: '#48AFF0',
        secondary: '#e0f',
        yellow: 'rgb(255, 214, 10)',
        red: '#ff3b30',
        green: 'rgb(52, 199, 89)',
        muted: '#191919',
        brown: '#AC8E68',
        highlight: '#29112c',
        gray: '#999',
        purple: '#c0f',
        darkened: 'rgba(22,22,22,.5)'
    },
    fonts: {
        body: "system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', sans-serif",
        heading: 'inherit',
        monospace: 'Menlo, monospace'
    },
    fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 72],
    fontWeights: {
        body: 400,
        heading: 700,
        display: 900
    },
    lineHeights: {
        body: 1.5,
        heading: 1.25,
        button: 1.15
    },
    breakpoints: ['350px', '700px', '980px'],
    radii: [3, 4, 6],
    shadows: {},
    sizes: [0, 5, 10, 15, 20, 30, 40, 50, 60],
    space: [0, 5, 10, 15, 20, 30, 40, 50, 60],
    zIndices: {},
    textStyles: {
        heading: {
            fontFamily: 'heading',
            fontWeight: 'heading',
            lineHeight: 'heading'
        },
        body: {
            fontFamily: 'body',
            fontWeight: 'body',
            fontSize: [0, 1, 2]
        },
        small: {
            fontFamily: 'body',
            fontWeight: 'body',
            fontSize: [0, 1]
        },
        display: {
            variant: 'textStyles.heading',
            fontSize: [5, 6],
            fontWeight: 'display',
            letterSpacing: '-0.03em',
            mt: 4
        }
    },
    messages: {},
    alerts: {
        danger: {
            color: 'text',
            bg: 'red'
        },
        success: {
            color: 'text',
            bg: 'green'
        }
    },
    buttons: {
        primary: {
            color: 'text',
            display: 'inline-flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            border: 'none',
            borderRadius: 1,
            cursor: 'pointer',
            px: 2,
            py: 1,
            verticalAlign: 'middle',
            lineHeight: 'button',
            textAlign: 'left',
            fontSize: 1,
            minWidth: 5,
            minHeight: 5,
            backgroundColor: t => t.colors.background,
            backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.05),hsla(0,0%,100%,0))',
            boxShadow: '0 0 0 1px rgba(16,22,26,.4)',
            '&::selection': {
                background: 'rgba(125,188,255,.6)'
            },
            '&:hover': {
                backgroundColor: '#4f4f4f'
            },
            '&:active': {
                boxShadow: '0 0 0 1px rgba(16,22,26,.6),inset 0 1px 2px rgba(16,22,26,.2)',
                backgroundColor: '#444'
            },
            '&:disabled': {
                opacity: 0.65,
                cursor: 'not-allowed'
            }
        },
        submit: {
            variant: 'buttons.primary',
            backgroundColor: '#137CBD',
            '&:hover': {
                backgroundColor: '#137CBD'
            },
            '&:active': {
                backgroundColor: '#0E5A8A'
            }
        },
        submitCta: {
            variant: 'buttons.submit',
            width: '100%'
        },
        link: {
            variant: 'styles.a',
            background: 'none',
            cursor: 'pointer',
            py: 0,
            px: 1
        }
    },
    layout: {
        flexCentered: {
            display: 'flex',
            flexDirection: 'column',
            minHeight: '100vh',
            minWidth: '100%',
            justifyContent: 'center',
            alignItems: 'center'
        },
        flexCenterWithHeader: {
            variant: 'layout.flexCentered',
            minHeight: 'calc(100vh - 66px)'
        },
        content: {
            maxWidth: ['100%', '680px', '940px']
        },
        reset: {
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px'
        }
    },
    cards: {
        monoform: {
            width: ['100%', '350px'],
            padding: '30px 40px 30px',
            borderRadius: [0, 1],
            backgroundColor: 'midground',
            boxShadow: '0 0 0 1px rgba(16,22,26,.4),0 0 0 rgba(16,22,26,0),0 0 0 rgba(16,22,26,0)'
        },
        wideform: {
            variant: 'cards.monoform',
            borderRadius: [0, 1],
            width: ['100%', null, '680px']
        },
        monotext: {
            variant: 'cards.monoform',
            paddingTop: 1,
            paddingBottom: 3,
            paddingX: 6
        },
        palette: {
            padding: [0, 3],
            borderRadius: [0, 1],
            backgroundColor: 'midground',
            boxShadow: '0 0 0 1px rgba(16,22,26,.4),0 0 0 rgba(16,22,26,0),0 0 0 rgba(16,22,26,0)'
        }
    },
    forms: {
        bottom: {
            fontSize: 0,
            mt: 1,
            mb: null
        },
        label: {
            fontSize: 1,
            mb: 1,
            mt: null
        },
        input: {
            height: 5,
            padding: 2,
            border: 'none',
            fontSize: 2,
            background: t => t.colors.darkened,
            borderRadius: t => `${t.radii[1]}px`,
            boxShadow:
                '0 0 0 0 rgba(19,124,189,0),0 0 0 0 rgba(19,124,189,0),inset 0 0 0 1px rgba(16,22,26,.15),inset 0 1px 1px rgba(16,22,26,.2)',
            '&::selection': {
                background: 'rgba(125,188,255,.6)'
            }
            // background: ['blue', 'red', 'yellow']
        },
        select: {
            // lineHeight: '1',
            px: 2,
            py: 1,
            border: 'none',
            fontSize: 1,
            background: t => t.colors.darkened,
            borderRadius: t => `${t.radii[0]}px`,
            boxShadow:
                '0 0 0 0 rgba(19,124,189,0),0 0 0 0 rgba(19,124,189,0),inset 0 0 0 1px rgba(16,22,26,.15),inset 0 1px 1px rgba(16,22,26,.2)',
            '&::selection': {
                background: 'rgba(125,188,255,.6)'
            }
            // background: ['blue', 'red', 'yellow']
        },
        search: {
            variant: 'forms.input',
            px: 1,
            '&::placeholder': {
                color: t => t.colors.placeholder
            }
        },
        textarea: {
            variant: 'forms.input',
            height: '120px',
            fontFamily: 'body',
            maxWidth: '100%'
        }
    },
    links: {
        '&:visited': {
            color: 'primary'
        },
        reset: {
            textDecoration: 'none'
        },
        textDecoration: 'none',
        nav: {
            fontWeight: 'bold',
            color: 'inherit',
            textDecoration: 'none'
        },
        cta: {
            variant: 'buttons.submit',
            textDecoration: 'none'
        }
    },
    badges: {
        tag: {
            variant: 'textStyles.small'
        },
        muted: {
            variant: 'badges.tag',
            backgroundColor: 'rgba(138,155,168,.2)'
        },
        selected: {
            variant: 'badges.tag',
            backgroundColor: 'colors.blue'
        }
    },
    styles: {
        Container: {
            p: 4,
            maxWidth: 1024
        },
        root: {
            variant: 'textStyles.body',
            margin: 0,
            padding: 0,
            lineHeight: 'body'
        },
        h1: {
            variant: 'textStyles.display',
            fontSize: [4, 5, 6]
        },
        h2: {
            variant: 'textStyles.heading',
            fontSize: [2, 3, 5]
        },
        h3: {
            variant: 'textStyles.heading',
            fontSize: [2, 3, 4]
        },
        h4: {
            variant: 'textStyles.heading',
            fontSize: [1, 2, 3]
        },
        h5: {
            variant: 'textStyles.heading',
            fontSize: [1, 2]
        },
        h6: {
            variant: 'textStyles.heading',
            fontSize: [0, 1]
        },
        a: {
            color: 'primary',
            textDecoration: 'none',
            '&:visited': {
                color: 'primary'
            }
        },
        pre: {
            variant: 'prism',
            fontFamily: 'monospace',
            fontSize: 1,
            p: 4,
            color: 'text',
            bg: 'muted',
            overflow: 'auto',
            code: {
                color: 'inherit'
            }
        },
        code: {
            fontFamily: 'monospace',
            color: 'secondary',
            fontSize: 1
        },
        inlineCode: {
            fontFamily: 'monospace',
            color: 'secondary',
            bg: 'muted'
        },
        table: {
            width: '100%',
            my: 1,
            borderSpacing: 0,
            fontSize: 1,
            'th,td': {
                textAlign: 'left',
                py: 0,
                borderColor: 'muted',
                borderBottomStyle: 'solid',
                borderBottomWidth: '1px',
                borderBottomColor: 'gray'
            },
            thead: {
                backgroundColor: 'background'
            },
            td: {
                verticalAlign: 'top'
            },
            th: {
                verticalAlign: 'bottom'
            }
        },
        hr: {
            border: 0,
            borderBottom: '1px solid',
            borderColor: 'muted'
        },
        img: {
            maxWidth: '100%'
        }
    }
};
