import React from "react";
import ReactDOM from "react-dom";
import theme from './theme';
import {ThemeProvider} from "theme-ui";
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import {Home} from "./components/home";
import {Film} from "./components/film";

var mountNode = document.getElementById("app");

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <div>
            <Router>
                <Switch>
                    <Route exact path='/'>
                        <Home/>
                    </Route>
                    <Route path='/film/:id'>
                        <Film/>
                    </Route>
                    <Route path='/'>
                        <Redirect to="/" />
                    </Route>
                </Switch>
            </Router>
        </div>
    </ThemeProvider>,
    mountNode
);