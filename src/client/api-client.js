const axios = require('axios');

export function getOrSearchFilms(params) {
    return axios.get('/api/films', {params}).then(res => res.data)
}

export function getFilm(id) {
    return axios.get(`/api/film/${id}`).then(res => res.data)
}