/** @jsx jsx */

import React, {useEffect, useState} from "react";
import {getOrSearchFilms} from '../api-client'
import InfiniteScroll from 'react-infinite-scroller';
import {Container, jsx, Styled, Spinner, Box} from "theme-ui";
import {Link} from "react-router-dom";
import {Search} from "./search";
import {Flex} from "@theme-ui/components";

export function Home() {

    const [films, setFilms] = useState([]);
    const [hasNext, setHasNext] = useState(true);
    const [loading, setLoading] = useState(false);
    const [params, setParams] = useState({query: undefined, page: 1});

    const pushPage = res => {
        setFilms(films => res.page === 1 ? res.results : [...films, ...res.results]);
        setHasNext(res.page < res.total_pages)
        setLoading(false);
    };

    useEffect(() => {
        setLoading(true);
        getOrSearchFilms(params).then(pushPage)
    }, [params]);

    const handleScrollEnd = () => {
        if (hasNext) {
            setParams(({query, page}) => ({query, page: page + 1}))
        }
    };

    const handleSearch = query => {
        setParams({query, page: 1});
    };

    const handleSearchReset = () => {
        setParams({query: undefined, page: 1});
    };

    const FilmItem = (film) => {
        return <Link key={film.id} css={{lineHeight: 0, userSelect: 'none'}} to={{pathname: `/film/${film.id}`}}>
            {film.poster_path ? <img sx={{
                minWidth: 0,
                maxWidth: '100%',
                height: 'auto',
                objectFit: 'contain',
            }} src={`https://image.tmdb.org/t/p/w500${film.poster_path}`} alt={film.title}/> : <Flex sx={{
                height: '100%',
                lineHeight: 'heading',
                alignItems: 'center',
                justifyContent: 'center',
                textAlign: 'center',
                backgroundColor: 'text'
            }}>{film.title}</Flex>}
        </Link>
    };
    const LoadingSpinner = () => {
        return <div sx={{
            gridColumn: ['1 / 4', '1 / 4', '1 / 5'],
            height: '100%',
            alignItems: 'center',
            justifyContent: 'center',
            textAlign: 'center',
            backgroundColor: 'darkened'
        }}><Spinner/></div>
    };
    return (
        <div>
            <Container variant='content'>
                <Container sx={{textAlign: 'center', p: 6}}>
                    <Styled.h2>Film DB</Styled.h2>
                </Container>
            </Container>
            <Container variant='content'>
                <Search search={handleSearch} reset={handleSearchReset}/>
            </Container>
            <Container variant='content'>
                <Styled.h3>{params.query ? `Films Matching "${params.query}"` : 'Most Popular Films'}</Styled.h3>
                <InfiniteScroll
                    pageStart={1}
                    loadMore={handleScrollEnd}
                    hasMore={hasNext && !loading}
                    loader={<LoadingSpinner key='loading-scroll'/>}
                    threshold={800}
                    initialLoad={false}
                    sx={{
                        padding: [1, 1, 2],
                        background: 'rgba(0,0,0,0.8)',
                        display: 'grid',
                        gridGap: [1, 1, 2],
                        gridTemplateColumns: [
                            'repeat(3, 1fr)',
                            'repeat(3, 1fr)',
                            'repeat(4, 1fr)'
                        ]
                    }}
                >
                    {
                        films && films.length > 0
                            ? films.map(FilmItem)
                            : (<span>No Films found</span>)
                    }
                </InfiniteScroll>

            </Container>
        </div>
    );
}