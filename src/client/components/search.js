/** @jsx jsx */
import React, {useState} from 'react';
import {Input, jsx, Label} from 'theme-ui';

export function Search(props) {
    const {reset, search} = props;
    const [query, setQuery] = useState('');

    const handleChange = (e) => {
        e.preventDefault();
        const q = e.target.value.trim();
        setQuery(q);
        if (q.length === 0) {
            reset();
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        search(query);
    };

    return <form onSubmit={handleSubmit} sx={{
        mx: 1,
        padding: 0,
        width: "320px",
        margin: '0 auto'
    }}>
        <Label htmlFor='search' sx={{display: 'none'}}>Search</Label>
        <Input variant='forms.search' name='search' type='search' onChange={handleChange} placeholder="Search Films"/>
    </form>;
}