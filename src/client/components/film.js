/** @jsx jsx */

import {useHistory, useParams} from "react-router";
import {useEffect, useState} from "react";
import {getFilm} from "../api-client";
import {Box, Container, Flex, jsx} from "theme-ui";


export function Film() {
    const { id } = useParams();
    const [film, setFilm] = useState({});
    const history = useHistory();
    useEffect(() => { getFilm(id).then(setFilm).catch(_ => history.push('/')) },[id]);

    return <Container variant='content'>
        <div sx={{
            display: 'grid',
            mx: [2, 2, 4],
            gridTemplateColumns: ['1fr', null, '1fr 1fr'],
            columnGap: [2, 2, 4],
            gridTemplateAreas: [
                `"title" ${film.poster_path ? '"film" ' : ''} "overview"`,
                null,
                `"title title" "${film.poster_path ? 'film' : 'overview'} overview"`
            ]
        }}>

            <Flex sx={{gridArea: 'title', alignItems: 'baseline'}}>
                <Box sx={{flexGrow: 1}}><h1>{film.title}</h1></Box>
            </Flex>

            {film.poster_path ? <Box sx={{gridArea: 'film'}}>
                <img sx={{
                    display: 'block',
                    margin: 0,
                    minWidth: 0,
                    maxWidth: '100%',
                    height: 'auto',
                    objectFit: 'cover'
                }} src={`https://image.tmdb.org/t/p/w500${film.poster_path}`} alt={film.title}/>
            </Box>:undefined}

            <Box sx={{gridArea: 'overview'}}>
                <p sx={{mt: [undefined, undefined, '-6.5px']}}>{film.overview}</p>
                <p>
                    <ul>
                        <li>Rating: {film.vote_average}</li>
                        <li>Release Date: {film.release_date}</li>
                    </ul>
                </p>
            </Box>
        </div>
    </Container>;
}