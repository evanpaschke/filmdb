class ConfigError {
    constructor(message) {
        this.message = message;
    }
}
exports = ConfigError;