class MovieDbError {
    constructor(status, message) {
        this.status = status;
        this.message = message;
    }
}

exports.MovieDbError = MovieDbError;