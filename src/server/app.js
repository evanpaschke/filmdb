const express = require('express');
const logger = require('morgan');
const path = require('path');
const {validator, validate} = require('./utils/validator');
const { getOrSearchFilms, getFilm } = require("./movie-client");
const { MovieDbError } = require('./errors/movie-db-error');

const clientBuildPath = path.join(__dirname, '../../dist');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(clientBuildPath));

const router = express.Router();

router.get('/api/films', validate, (req, res, next) => {
    getOrSearchFilms(req.query).then(d => res.send(d)).catch(next);
});

router.get('/api/film/:id', validate, (req, res, next) => {
    getFilm(req.params.id).then(d => res.send(d)).catch(next);
});

router.get('*', (req, res, next) => {
    res.sendFile(path.join(clientBuildPath, "index.html"));
});

app.use('/', router);
app.use(validator);
app.use((err, req, res, next) => {
    if (err instanceof MovieDbError) {
        return res.status(err.status).json({message: err.message});
    }
    next(err)
});
app.use((err, req, res, next) => {
    console.log(err);
    return res.status(500).json({message: 'Internal Server Error'});
});

module.exports = app;