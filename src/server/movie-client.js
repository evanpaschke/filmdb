const {setupCache} = require("axios-cache-adapter");

const {MovieDbError} = require("./errors/movie-db-error");
const {create} = require('axios');
const {config} = require('./config');

const params = {'api_key': config.filmdb.apiKey};

const cache = setupCache({
    maxAge: 60 * 60 * 1000, // 1hr cache
    exclude: {query: false},
    key: req => {
        // omit api_key from cache key
        const params = Object.keys(req.params).filter(k => k !== 'api_key')
            .reduce((params, key) => {
                params.append(key, req.params[key]);
                return params
            }, new URLSearchParams());
        return `${req.url}?${params.toString()}`
    }
});

const movieDbApi = create({baseURL: config.filmdb.baseUrl, adapter: cache.adapter});
movieDbApi.interceptors.request.use(config => {
    config.params = {...params, ...config.params};
    return config;
});

function mapErrorResponse(err) {
    const {status, data} = err.response;
    throw new MovieDbError(status, data.status_message);
}

function getOrSearchFilms(params) {
    const {query} = params;
    return query
        ? searchFilms(params)
        : getFilms(params);
}

function getFilm(id) {
    return movieDbApi.get(`/movie/${id}`)
        .then(res => res.data).catch(mapErrorResponse)
}

function searchFilms(params) {
    return movieDbApi.get('/search/movie', {params})
        .then(res => res.data).catch(mapErrorResponse)
}

function getFilms(params) {
    return movieDbApi.get('/movie/popular', {params})
        .then(res => res.data).catch(mapErrorResponse)
}

exports.getOrSearchFilms = getOrSearchFilms;
exports.getFilm = getFilm;
