const ConfigError = require('./errors/config-error');

/*
* Expects env var keys in the form FILMDB_${NODE_ENV}_${KEY}:
* FILMDB_API_KEY
* FILMDB_BASE_URL
* */

function resolveEnvConfig(key) {
    const nodeEnv = process.env.NODE_ENV && process.env.NODE_ENV.toUpperCase();
    return process.env['FILMDB_' + [nodeEnv, key].filter(k=>k).join('_')];
}

const config = {
    filmdb: {
        apiKey: resolveEnvConfig('API_KEY'),
        baseUrl: resolveEnvConfig('BASE_URL')
    }
};

if (Object.values(config.filmdb).some(v => !v)) {
    throw new ConfigError('missing filmdb config needed to start the app');
}

exports.config = config;