const {init, validate, InputValidationError, } = require('openapi-validator-middleware');

init('./openapi.yml');

module.exports = {
    validator: (err, req, res, next) => {
        if (err instanceof InputValidationError) {
            return res.status(400).json({errors: err.errors});
        }
        next(err)
    },
    validate
};