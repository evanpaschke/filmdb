FROM node:12

COPY . /src

WORKDIR /src

RUN npm install
RUN npm run build:ui

CMD ["npm", "start"]
